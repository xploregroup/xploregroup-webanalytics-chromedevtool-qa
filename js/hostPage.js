/*
* Hostpage functions
*/
function getNamespacedEventName(name) {
  return NAMESPACE + '-' + name;
}

function getDtmRules() {
  String.prototype.htmlEncode = function () {
    return String(this)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\\/g, '&#92;')
        .replace(/\//g, '&#47;');
  }

  function prepareDtmRules(ruleSet) {
    var returnRules = [];
    for (var i=0; i < ruleSet.length; i++) {
      var data = ruleSet[i];

      var ruleData = {
        "customEvent": data.customEvent,
        "name": data.name,
        "event": data.event
      };

      if (data.hasOwnProperty('trigger')) {
        ruleData.trigger = [];
        for (var j=0; j < data.trigger.length; j++) {
          ruleData.trigger.push({"engine" : data.trigger[j].engine, "command": data.trigger[j].command});
        }
      }

      if (data.hasOwnProperty('scope')) {
        ruleData.scope = [];
        for (var p in data.scope) {
          if( data.scope.hasOwnProperty(p) ) {
            ruleData.scope.push(p + ": " + JSON.stringify(data.scope[p], null, ' '));
          }
        }
      }

      if (data.hasOwnProperty('conditions')) {
        ruleData.conditions = [];
        for (var j=0; j < data.conditions.length; j++) {
          ruleData.conditions.push("<pre lang='javascript'><code>" + data.conditions[j].toString().htmlEncode() + "</code></pre>");
        }
      }

      returnRules.push(ruleData);
    }
    return returnRules;
  }

  if (typeof _satellite === "object") {
    if (_satellite.rules.length != 0) {
      var dtmRules = prepareDtmRules(_satellite.rules);
      return(dtmRules);
    }
    else {
      return "notdefined";
    }
  }
  return "";
}

function getAnalyticsData() {

  function _randomString(length) {
      var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

      if (! length) {
          length = Math.floor(Math.random() * chars.length);
      }

      var str = '';
      for (var i = 0; i < length; i++) {
          str += chars[Math.floor(Math.random() * chars.length)];
      }
      return str;
  }

  function _getAnalyticsData() {
      var eventList = [];

      var allEvents = document.querySelectorAll('[data-tracking-event]');
      for (var i=0; i<allEvents.length; i++) {
        var randomID = _randomString(8);
        allEvents[i].setAttribute('data-tracking-qa-dtm', randomID);
        var trackingEvent = allEvents[i].getAttribute('data-tracking-event');
        var trackingInfo = allEvents[i].getAttribute('data-tracking-info');
        var trackingCommerce = allEvents[i].getAttribute('data-tracking-commerce');

        eventList.push({'trackingEvent': trackingEvent, 'trackingInfo': trackingInfo, 'trackingCommerce': trackingCommerce, 'trackingQaDtm' : randomID});
      }

      return eventList;
  };

  return _getAnalyticsData();
}

function getTriggerReference() {
  return availableEvents;
}

function isDTMinstalled() {
  return (typeof _satellite === "object");
}

function isGTMinstalled() {
  return (typeof google_tag_manager === "object");
}
