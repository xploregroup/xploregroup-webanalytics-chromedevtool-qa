var connections = {};
var clearLogOnRefresh = false;

chrome.runtime.onConnect.addListener(function (port) {

    var extensionListener = function (message, sender, sendResponse) {

        // The original connection event doesn't include the tab ID of the
        // DevTools page, so we need to send it explicitly.
        if (message.name == "init") {
          connections[message.tabId] = port;
          connections[message.tabId].postMessage({"name": "extension-connected"});
          chrome.tabs.executeScript(message.tabId, {
            file: 'js/content.js'
          });
          chrome.storage.sync.get('enableDTM', function (items) {
            connections[message.tabId].postMessage({"name": "enable-dtm", "ruleSettings" : items});
          });
          chrome.storage.sync.get('enableGTM', function (items) {
            connections[message.tabId].postMessage({"name": "enable-gtm", "ruleSettings" : items});
          });
          chrome.storage.local.get("GTMTriggers", function (items) {
            connections[message.tabId].postMessage({"name": "gtm-rules", "eventData" : items});
          });
          return;
        }

        if (message.name == "refresh-page") {
           // Tab's location had changed and the page confirmed that it was a refresh.
           // Start the content script.
           chrome.tabs.executeScript(message.tabId, {
             file: 'js/content.js'
           });
        }

        if (message.name == "clear-log-on-refresh") {
           // Tab's location had changed and the page confirmed that it was a refresh.
           // Start the content script.
           clearLogOnRefresh = true;
        }

        if (message.name == "keep-log-on-refresh") {
           // Tab's location had changed and the page confirmed that it was a refresh.
           // Start the content script.
           clearLogOnRefresh = false;
        }

        if (message.name == "clear-eventlog") {
           // Tab's location asks for reset of eventLog.
           chrome.tabs.sendMessage(message.tabId, {
             name: 'resetLog'
           });
        }

        if (message.name == "gtm-get-accounts") {
          loadGTMApi(function (res) {
            selectAccount(function (res) {
              connections[message.tabId].postMessage({"name": "gtm-select-account", "data" : res});
            })});
        }

        if (message.name == "gtm-get-containers") {
          selectContainer( message.accountId, function (res) {
            connections[message.tabId].postMessage({"name": "gtm-select-container", "data" : res});
          });
        }

        if (message.name == "gtm-get-workspaces") {
          selectWorkspace( message.containerId, function (res) {
            connections[message.tabId].postMessage({"name": "gtm-select-workspace", "data" : res});
          });
        }

        if (message.name == "gtm-get-triggers") {
          getTagsAndTriggers( message.workspaceId, function (res) {
            connections[message.tabId].postMessage({"name": "gtm-tags-and-triggers", "data" : res});
          });
        }
    }

    // Listen to messages sent from the DevTools page
    port.onMessage.addListener(extensionListener);

    port.onDisconnect.addListener(function(port) {
        port.onMessage.removeListener(extensionListener);

        var tabs = Object.keys(connections);
        for (var i=0, len=tabs.length; i < len; i++) {
          if (connections[tabs[i]] == port) {
            // Send a message to the content script do necessary clean-up
            chrome.tabs.sendMessage(parseInt(tabs[i]), {
              name: 'clean-up'
            });
            delete connections[tabs[i]]
            break;
          }
        }
    });
});

// Receive message from content script and relay to the devTools page for the
// current tab
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    // Messages from content scripts should have sender.tab set
    if (sender.tab) {
      var tabId = sender.tab.id;
      if (tabId in connections) {
        connections[tabId].postMessage(request);
      } else {
        console.log("Tab not found in connection list.");
      }
    } else {
      console.log("sender.tab not defined.");
    }
    return true;
});

chrome.webNavigation.onDOMContentLoaded.addListener(function(details) {
      if (details.frameId !== 0) {
        // If it is not the top-frame, we just ignore it.
        return;
      }

      var tabId = details.tabId;
      if (tabId in connections) {
        connections[tabId].postMessage({
          name: 'refresh-completed'
        });
      }
});

chrome.webNavigation.onBeforeNavigate.addListener(function(details) {
      if (details.frameId !== 0) {
        // If it is not the top-frame, we just ignore it.
        return;
      }

      if (clearLogOnRefresh) {
        var tabId = details.tabId;

        chrome.tabs.sendMessage(tabId, {
          name: 'resetLog'
        });
      }
});
