function getNamespacedEventName(name) {
  return chrome.runtime.id + '-' + name;
}

function receiveMessage(event)
{
  if (event && event.data === "analytics-track-change") {
    try {
      var eventLog = window.sessionStorage.getItem('eventLog');
      chrome.runtime.sendMessage({'name': 'content-event', 'log' : eventLog}, function(response) {
        // console.log(response);
      });
    } catch (e) {}
  }
}

window.addEventListener("message", receiveMessage, false);

try {
  var eventLog = window.sessionStorage.getItem('eventLog');
  chrome.runtime.sendMessage({'name': 'content-event', 'log' : eventLog}, function(response) {
      // console.log(response);
  });
} catch (e) {
  chrome.runtime.sendMessage({'name': 'content-nosessionstorage', 'log' : "sessionStorage is disbaled; the tool requires sessionStorage to be enabled! This tool is only for debugging purposes."}, function(response) {
    // console.log(response);
  });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    // Messages from content scripts should have sender.tab set
    if (! sender.tab) {
      if (request.name == "resetLog") {
        if (typeof window.sessionStorage !== "undefined") {
          window.sessionStorage.setItem('eventLog', "");
        }
      }
    } else {
      console.log("sender.tab should not be defined.");
    }
    return true;
});
