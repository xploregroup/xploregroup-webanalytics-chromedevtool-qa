/* !
 * JSON Validator 1.0.0 - 19.05.2016
 * Copyright (c) 2016, Vadym Danyliuk <VadymDanyliuk@gmail.com>
 * GitHub: https://github.com/VadymDanyliuk/json-validator#readme
 * License: MIT
 */

(function () {
    'use strict';

    JSON.Validator = {
        validate: function (json, pattern) {
            if (!json || !pattern || pattern.constructor != Object) {
                return null;
            }

            this._json = json;
            this._pattern = pattern;

            this._failMessages = [];
            this._successMessages = [];
            this._requireChar = '*';
            this._reservedProps = ['__settings__', '__list__'];

            this._tryToParse();

            pattern.__settings__ = pattern.__settings__ || {};
            pattern.__settings__.name = 'root';

            if (this._json) {
                this._checkPattern(this._pattern, this._json);
            }

            return {
                isValid: !this._failMessages.length,
                failMessages: this._failMessages,
                successMessages: this._successMessages
            };
        },
        _checkPattern: function (pattern, json) {
            var isArray = this._hasArrayType(pattern) && json.constructor == Array;

            this._checkTags(pattern, json);
            this._checkRules(pattern, json);

            if (isArray) {
                for (var i = 0; i < json.length; i++) {
                    this._checkChildren(pattern, json[i]);
                    this._checkType(pattern, json);
                    this._checkValue(pattern, json[i]);
                }
            } else {
                this._checkChildren(pattern, json);
                this._checkType(pattern, json);
                this._checkValue(pattern, json);
            }
        },
        _checkType: function (pattern, json) {
            for (var i = 0; i < pattern.__settings__.type.length; i++) {
                if (pattern.__settings__.type[i] == json.constructor) {
                    this._successMessages.push('Type of #' + pattern.__settings__.name + ' is correct');
                    return true;
                }
            }

            this._failMessages.push('Wrong type of #' + pattern.__settings__.name);
        },
        _checkChildren: function (pattern, json) {
            if (!pattern.__settings__.contains || !pattern.__settings__.contains.length) {
                return true;
            }

            for (var i = 0; i < pattern.__settings__.contains.length; i++) {
                var prop = pattern.__settings__.contains[i];

                var isRequire = prop.charAt(0) == this._requireChar;
                if (isRequire) {
                    prop = prop.slice(1);
                }

                if (pattern[prop]) {
                    pattern[prop].__settings__.name = prop;
                }

                var isAvailable = json.hasOwnProperty(prop);
                if (isRequire && !isAvailable) {
                    this._failMessages.push('Property #' + prop + ' of #' + pattern.__settings__.name + ' isn\'t exist');
                    continue;
                }

                if (!isAvailable) {
                    continue;
                }

                this._successMessages.push('Property #' + prop + ' of #' + pattern.__settings__.name + ' is available');

                this._checkPattern(pattern[prop], json[prop]);
            }
        },
        _checkValue: function (pattern, json) {
            if (!pattern.__settings__.func || pattern.__settings__.func.constructor != Function) {
                return true;
            }

            if (!pattern.__settings__.func.call(this, json)) {
                this._failMessages.push('Value of #' + pattern.__settings__.name + ' is incorrect');
            } else {
                this._successMessages.push('Value of #' + pattern.__settings__.name + ' is correct');
            }
        },
        _checkRules: function (pattern, json) {
            if (!pattern.__settings__.rule || !pattern.__settings__.rule.length) {
                return true;
            }

            var result = [];
            for (var i = 0; i < pattern.__settings__.rule.length; i++) {
                switch (pattern.__settings__.rule[i]) {
                    case '__notempty__':
                        if (json.constructor == Array) {
                            var condition = json.length > 0;
                            result.push({
                                result: condition,
                                rule: pattern.__settings__.rule[i]
                            });
                        }

                        break;
                }
            }

            for (var j = 0; j < result.length; j++) {
                if (result[j].result) {
                    this._successMessages.push('Rule ' + result[j].rule + ' of #' + pattern.__settings__.name + ' has followed');
                } else {
                    this._failMessages.push('Rule ' + result[j].rule + ' of #' + pattern.__settings__.name + ' has ignored');
                }
            }
        },
        _checkTags: function (pattern, json) {
            if (!pattern.__settings__.contains || !pattern.__settings__.contains.length) {
                return true;
            }

            for (var i = 0; i < pattern.__settings__.contains.length; i++) {
                switch (pattern.__settings__.contains[i]) {
                    case '__list__':
                        pattern.__settings__.contains.splice(i, 1);

                        for (var prop in json) {
                            if (!json.hasOwnProperty(prop)) {
                                continue;
                            }

                            if (this._reservedProps.indexOf(prop) >= 0) continue;

                            pattern.__settings__.contains.push(prop);
                            pattern[prop] = this._cloneObject(pattern.__list__);
                        }

                        delete pattern.__list__;
                        break;
                }
            }
        },
        _tryToParse: function () {
            if (this._json.constructor == Object) {
                return true;
            }

            try {
                this._json = JSON.parse(this._json);
                this._successMessages.push('JSON parsed successfully');
            } catch (ex) {
                this._json = false;
                this._failMessages.push('Parsing JSON failed');
            }
        },
        _cloneObject: function (object) {
            if (object === null || typeof (object) !== 'object' || 'isActiveClone' in object) {
                return object;
            }

            var temp = object.constructor();
            for (var key in object) {
                if (Object.prototype.hasOwnProperty.call(object, key)) {
                    object.isActiveClone = null;
                    temp[key] = this._cloneObject(object[key]);
                    delete object.isActiveClone;
                }
            }

            return temp;
        },
        _hasArrayType: function (pattern) {
            var constructor = [].constructor;
            for (var i = 0; i < pattern.__settings__.type.length; i++) {
                if (constructor == pattern.__settings__.type[i]) {
                    return true;
                }
            }

            return false;
        }
    };

})();
