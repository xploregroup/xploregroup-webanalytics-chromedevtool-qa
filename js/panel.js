(function() {

  var rulesSetDTM = [];
  var rulesSetGTM = [];
  var availableEvents = [];
  var cex_installed = false;
  // For injecting code into the host page
  var EvalHelper;

  var hasDTM = false;
  var hasGTM = false;

  var enableGTM = false;
  var enableDTM = false;

  /*
  * GTM ruleset API selectors
  */
  var accountSelector = document.getElementById('gtm-account-selector');
  var containerSelector = document.getElementById('gtm-container-selector');
  var workspaceSelector = document.getElementById('gtm-workspace-selector');

  $('#myevents .triggerlist li').remove();
  $('#mydtmddl .triggerlist li').remove();
  $('#mygtmddl .triggerlist li').remove();

  /*
   * Some helper functions
   */
  var ruleDefined = function(rulename, ruleset) {
  	var foundRule = false;

  	$.each(ruleset, function isIn (index, item) {
  		if ((item.event == "custom") && (item.customEvent == rulename)) {
  			foundRule = true;
  			return false; // break the loop
  		}
  	});
  	return foundRule;
  }

  var eventDefined = function(eventname, eventset) {
  	var foundEvent = "";

  	$.each(eventset, function isIn (index, item) {
  		if (item.event == eventname) {
  			foundEvent = item.rule;
  			return false; // break the loop
  		}
  	});
  	return foundEvent;
  }

  var eventQA = function(eventname, eventset) {
  	var foundEvent = "";

  	$.each(eventset, function isIn (index, item) {
  		if (item.event == eventname) {
  			foundEvent = item;
  			return false; // break the loop
  		}
  	});
  	return foundEvent;
  }

  var triggerTools = function (tcode, command) {
    if (command == "loadScript") {return "Custom tag - Javascript"};
    switch(tcode) {
      case "sc" : return "Adobe Analytics: " + command;
      case "ga" : return "Google Analytics:" + command;
      case "ga_universal" : return "Universal Analytics:" + command;
      case "visitor_id" : return "Marketing Cloud ID servive";
    }
    return "unknown";
  }

  Object.defineProperty( Object.prototype, "has", { value: function( needle ) {
      var obj = this;
      var needles = needle.split( "." );
      for( var i = 0; i<needles.length; i++ ) {
          if( !obj.hasOwnProperty(needles[i])) {
              return false;
          }
          obj = obj[needles[i]];
      }
      return true;
  }});

  function checkJSON(jsondata) {
    var okJSON;
    try {
      okJSON = JSON.parse(jsondata);
    } catch (e) {return false;}

    return okJSON;
  }

  function checkFieldsPresent(event, datasetId, dataset, QAset) {
  	var qaEntry = eventQA(event, QAset);

  	if (qaEntry) {
  		var requiredFields = qaEntry[datasetId];

  		var fieldsFound = true;

      var fieldsError = "";

      var dataObject = checkJSON(dataset);

      if (dataObject === false) {
        // JSON syntax error
        return "JSON syntax";
      } else {
        $.each(requiredFields, function isOK (index, item) {
          if (item.indexOf("*") != -1) {
            // required field
            if (! dataObject.has(item.replace("*", ""))) {
      				fieldsFound &= false;
              fieldsError += item + " ";
      			}
          }
    		});

        if (! fieldsFound) {
          return fieldsError;
        } else {
          return false;
        }
      }
  	}

  	return false;
  }

  function randomString(length) {
      var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

      if (! length) {
          length = Math.floor(Math.random() * chars.length);
      }

      var str = '';
      for (var i = 0; i < length; i++) {
          str += chars[Math.floor(Math.random() * chars.length)];
      }
      return str;
  }

  function init() {
    createEvalHelper(function(helper) {
        EvalHelper = helper;
        EvalHelper.defineFunctions([{
          name: 'getNamespacedEventName',
          string: getNamespacedEventName.toString()
          },{
            name: 'getDtmRules',
            string: getDtmRules.toString()
          },{
            name: 'getTriggerReference',
            string: getTriggerReference.toString()
          },{
            name: 'getAnalyticsData',
            string: getAnalyticsData.toString()
          },{
            name: 'isDTMinstalled',
            string: isDTMinstalled.toString()
          }
          ,{
            name: 'isGTMinstalled',
            string: isGTMinstalled.toString()
          }
          ], function(result, error) {
            EvalHelper.executeFunction('isDTMinstalled', [], function (result, error) {
              hasDTM = result;
              setPanelTabsDTM();
            });
            EvalHelper.executeFunction('isGTMinstalled', [], function (result, error) {
              hasGTM = result;
              setPanelTabsGTM();
            });
            EvalHelper.executeFunction('getTriggerReference', [], function (result, error) {
                if (error) {
                  throw error;
                } else {
                  setTriggerReference(result);
                  if (hasDTM) {
                    EvalHelper.executeFunction('getDtmRules', [], function (result, error) {
                        if (error) {
                          throw error;
                        } else {
                          setDtmRulesSet(result);
                        }
                    });
                  }
                  EvalHelper.executeFunction('getAnalyticsData', [], function (result, error) {
                      if (error) {
                        throw error;
                      } else {
                        try {
                          setAnalyticsData(result);
                        } catch(e) {
                          console.log(e);
                        }
                      }
                  });
                }
            });
          });
    });
  }

  function setPanelTabsGTM() {
    if (hasGTM && enableGTM) {
      $( "#tabs" ).tabs("enable", "#tabs-gtmrules");
    } else {
      $( "#tabs" ).tabs("disable", "#tabs-gtmrules");
    }
  }

  function setPanelTabsDTM() {
    if (hasDTM && enableDTM) {
      $( "#tabs" ).tabs("enable", "#tabs-dtmrules");
    } else {
      $( "#tabs" ).tabs("disable", "#tabs-dtmrules");
    }
  }

  function processEvents(eventLog) {
    // console.log(eventLog);
    $('#myevents .triggerlist li').remove();
    eventLog = "[" + eventLog.substring(eventLog.indexOf("{")) + "]";
    allEvents = JSON.parse(eventLog);
    for (var i=0; i < allEvents.length; i++) {
      var randomID = randomString(8);

      $('#myevents .triggerlist').append('<li style="color:darkblue"><p class="someeventpage">' + allEvents[i].event + '</p>' +
            '<div id="hideme_' + randomID + '" style="display:none; background: grey;"><pre style="color:#101010">eventData: ' + JSON.stringify(allEvents[i], null, ' ') + '</pre></div>' + '</li>');
    }
  }

  function reScanPage() {
    createEvalHelper(function(helper) {
        EvalHelper = helper;
        EvalHelper.defineFunctions([{
          name: 'getNamespacedEventName',
          string: getNamespacedEventName.toString()
          },{
            name: 'getAnalyticsData',
            string: getAnalyticsData.toString()
          },
          ], function(result, error) {
            EvalHelper.executeFunction('getAnalyticsData', [], function (result, error) {
                if (error) {
                  throw error;
                } else {
                try {
                  setAnalyticsData(result);
                } catch(e) {
                  console.log(e);
                }
              }
            });
          });
    });
  }

  function setTriggerReference (eventData) {
    $('#qamap .triggerlist li').remove();

    availableEvents = eventData;

    for (var i=0; i < availableEvents.length; i++) {
      var data = availableEvents[i];
      $('#qamap .triggerlist').append('<li style="color:green">' +
          data.event + ' --> ' + data.rule + '<ul>' +
          ((data.infoFields.length > 0) ? '<li style="color:blue"> <b>info</b>: ' + data.infoFields + '</li>' : '' ) +
          ((data.commerceFields && data.commerceFields.length > 0)? '<li style="color:darkblue"> <b>commerce</b>: ' + data.commerceFields  + '</li>' : '' ) + '</ul></li>');
    }
  }

  function setAnalyticsData (eventData) {
    $('#triggers .triggerlist li').remove();

    for (var i=0; i < eventData.length; i++) {
      var data = eventData[i];
      var mappedEvent = eventDefined(data.trackingEvent, availableEvents);
      var eventError = (mappedEvent === "");
      var infoError = (data.trackingInfo) ? checkFieldsPresent(data.trackingEvent, "infoFields", data.trackingInfo, availableEvents) : false;
      var commerceError = (data.trackingCommerce) ? checkFieldsPresent(data.trackingEvent, "commerceFields", data.trackingCommerce, availableEvents) : false;

      var dtmRule = ruleDefined(mappedEvent, rulesSetDTM);
      var gtmRule = ruleDefined(mappedEvent, rulesSetGTM);

       $('#triggers .triggerlist').append('<li style="color:'+ ((eventError) ? "red" : "black") + '">' +
          '<span class="triggerclick" id="' + data.trackingQaDtm + '">' + data.trackingEvent + '</span>' +
          ((hasDTM) ? '<span style="color:' +  ((dtmRule) ? "green" : "orange") + '"> --> [DTM] ' + mappedEvent + '</span>' : '') +
          ((hasGTM) ? ' <span style="color:' + ((gtmRule) ? "green" : "orange") + '"> --> [GTM] ' + mappedEvent + '</span>' : '') + '<ul>' +
          '<li style="color:' + ((infoError === false) ? "blue" : "red") + '"> <b>info</b>: ' + data.trackingInfo + '</li>' +
          ((data.trackingCommerce) ? (' <li style="color:' + ((commerceError === false) ? "darkblue" : "red") + '"> <b>commerce</b>: ' + data.trackingCommerce  + '</li>') : '') + '</ul>' +
          ((eventError || infoError || commerceError) ? '<span style="color:red;font-weight:bold"> ERROR on ' + ((eventError) ? " EVENT" : "") + ((infoError) ? " INFO " + infoError : "") + ((commerceError) ? " COMMERCE " + commerceError : "") + '</span>' : '') +
          '</li>');
    }
  }

  function setDtmRulesSet (eventData) {
      $('#dtmrules .triggerlist li').remove();

      if (eventData == "notdefined") {
        $('#dtmrules .triggerlist').append('<li style="color:red;"><b>No DTM Event rules defined yet</b></li>');
        return;
      } else if (eventData == "") {
        $('#dtmrules .triggerlist').append('<li style="color:red;"><b>No DTM found - check installation</b></li>');
        return;
      }

      rulesSetDTM = eventData;

      for (var i=0; i < rulesSetDTM.length; i++) {
        var data = rulesSetDTM[i];

        var dtmdata = "<ul>";

        if (data.hasOwnProperty("trigger")) {
            var triggerdata = "<ul>";

              for(var j=0; j<data.trigger.length; j++) {
                  triggerdata += "<li>" + triggerTools(data.trigger[j].engine, data.trigger[j].command) + "</li>";
              }
            triggerdata += "</ul>";
            dtmdata += "<li>triggers: " + triggerdata + "</li>";
        }

        if (data.hasOwnProperty("scope")) {
            var scopedata = "<ul>";

              for(var j=0; j<data.scope.length; j++) {
                  scopedata += "<li>" + data.scope[j] + "</li>";
              }
            scopedata += "</ul>";
            dtmdata += "<li>scope: " + scopedata + "</li>";
        }

        if (data.hasOwnProperty("conditions")) {
            var conddata = "<ul>";

              for(var j=0; j<data.conditions.length; j++) {
                  conddata += "<li>" + data.conditions[j] + "</li>";
              }
            conddata += "</ul>";
            dtmdata += "<li>conditions: " + conddata + "</li>";
        }

        dtmdata += "</ul>";
        $('#dtmrules .triggerlist').append('<li style="color:darkblue;"><b>' + data.customEvent + '</b>' + ' : ' + data.name + dtmdata + '</li>');
      }
  }

  function setGtmRuleSet(eventData) {
    $('#gtmrules .triggerlist li').remove();
    rulesSetGTM = eventData;
    for (var i=0; i < rulesSetGTM.length; i++) {
      var data = rulesSetGTM[i];
      $('#gtmrules .triggerlist').append('<li style="color:darkblue;"><b>' + data.customEvent + '</b>' + ' : ' + data.name + '</li>');
    }
  }

  // Create a connection to the background page
  var backgroundPageConnection = chrome.runtime.connect({
      name: "panel"
  });

  backgroundPageConnection.onMessage.addListener(function (message) {
    if (message.name == "extension-connected") {
        init();
    }

    if (message.name == "refresh-completed") {
      init();
      $('#contenterror').hide();
      
      backgroundPageConnection.postMessage({
          name: 'refresh-page',
          tabId: chrome.devtools.inspectedWindow.tabId
      });
    }

    if (message.name == "content-event") {
      processEvents(message.log);
    }

    if (message.name == "content-nosessionstorage") {
      $('#contenterror').text(message.log);
      $('#contenterror').show();
    }

    if (message.name == "enable-dtm") {
      enableDTM = message.ruleSettings.enableDTM;
    }
    if (message.name == "enable-gtm") {
      enableGTM = message.ruleSettings.enableGTM;
    }

    if (message.name == "gtm-rules") {
      $('#gtmrules .triggerlist li').remove();

      rulesSetGTM = message.eventData.GTMTriggers;

      for (var i=0; i < rulesSetGTM.length; i++) {
        var data = rulesSetGTM[i];
        $('#gtmrules .triggerlist').append('<li style="color:darkblue;"><b>' + data.customEvent + '</b>' + ' : ' + data.name + '</li>');
      }
    }

    if (message.name == "analytics-event") {
      var randomID = randomString(8);

      $('#myevents .triggerlist').append('<li style="color:darkblue"><p class="someeventpage">' + message.eventData.event + '</p>' +
            '<div id="hideme_' + randomID + '" style="display:none; background: grey;"><pre style="color:#101010">eventData: ' + JSON.stringify(message.eventData, null, ' ') + '</pre></div>' + '</li>');
    }

    /*
    * GTM connection messages
    */
    if (message.name == "gtm-select-account") {
      var accounts = message.data;

      accountSelector.innerHTML = '';

      var el = document.createElement("option");
      el.textContent = "[select account...]";
      el.value = "";
      accountSelector.appendChild(el);

      for (var item in accounts) {
        var el = document.createElement("option");
        el.textContent = accounts[item].name;
        el.value = accounts[item].accountId;
        accountSelector.appendChild(el);
      }

      $("#gtmAPIconnector").show();
    }

    if (message.name == "gtm-select-container") {
      var containers = message.data;

      containerSelector.innerHTML = '';

      var el = document.createElement("option");
      el.textContent = "[select container...]";;
      el.value = "";
      containerSelector.appendChild(el);

      for (var item in containers) {
        var el = document.createElement("option");
        el.textContent = containers[item].name;
        el.value = containers[item].containerId;
        containerSelector.appendChild(el);
      }
    }

    if (message.name == "gtm-select-workspace") {
      var workspaces = message.data;

      workspaceSelector.innerHTML = '';

      var el = document.createElement("option");
      el.textContent = "[select workspace...]";;
      el.value = "";
      workspaceSelector.appendChild(el);

      for (var item in workspaces) {
        var el = document.createElement("option");
        el.textContent = workspaces[item].name;
        el.value = workspaces[item].workspaceId;
        workspaceSelector.appendChild(el);
      }
    }

    if (message.name == "gtm-tags-and-triggers") {
      setGtmRuleSet(message.data);
    }
  });

  backgroundPageConnection.postMessage({
      name: 'init',
      tabId: chrome.devtools.inspectedWindow.tabId
  });

  $( document ).on( 'click', '.someeventpage', function () {
      $('.someeventpage').css( "font-weight", "normal" );
      $(this).css( "font-weight", "bold" );
      $(this).parent().find('div').toggle();
  });

  $( "#cleareventspage").click( function () {
    $('#myevents .triggerlist li').remove();
    backgroundPageConnection.postMessage({
        name: 'clear-eventlog',
        tabId: chrome.devtools.inspectedWindow.tabId
    });
  });

  $('#clearonload').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        backgroundPageConnection.postMessage({
            name: 'clear-log-on-refresh',
            tabId: chrome.devtools.inspectedWindow.tabId
        });
    } else {
        // the checkbox is now no longer checked
        backgroundPageConnection.postMessage({
            name: 'keep-log-on-refresh',
            tabId: chrome.devtools.inspectedWindow.tabId
        });
    }
});

  $( "#rescanpage").click( function () {
    reScanPage();
  });

  $( document ).on( 'click', '.triggerclick', function () {
    var thelement = $(this).attr("id");
    chrome.devtools.inspectedWindow.eval('inspect(document.querySelector("[data-tracking-qa-dtm=' + thelement + ']"))');
  });

  $( "#loadgtmruleset").click( function () {
    backgroundPageConnection.postMessage({
        name: 'gtm-get-accounts',
        tabId: chrome.devtools.inspectedWindow.tabId
    });
  });

  $( document ).on( 'change', '#gtm-account-selector', function (event) {
    backgroundPageConnection.postMessage({
        name: 'gtm-get-containers',
        tabId: chrome.devtools.inspectedWindow.tabId,
        accountId : event.target.value
    });
  });

  $( document ).on( 'change', '#gtm-container-selector', function (event) {
    backgroundPageConnection.postMessage({
        name: 'gtm-get-workspaces',
        tabId: chrome.devtools.inspectedWindow.tabId,
        containerId : event.target.value
    });
  });

  $( document ).on( 'change', '#gtm-workspace-selector', function (event) {
    backgroundPageConnection.postMessage({
        name: 'gtm-get-triggers',
        tabId: chrome.devtools.inspectedWindow.tabId,
        workspaceId : event.target.value
    });
  });
})();
