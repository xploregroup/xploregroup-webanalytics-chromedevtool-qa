// Enter an API key from the Google API Console:
//   https://console.developers.google.com/apis/credentials
var apiKey = 'AIzaSyAmK-_cBFhFjdIqv4gvSb7-beKWN8T4AKc';
// Enter a client ID for a web application from the Google API Console:
//   https://console.developers.google.com/apis/credentials?project=_
// In your API Console project, add a JavaScript origin that corresponds
//   to the domain where you will be running the script.
var clientId = '320902054476-87slqd75191180hto07do0imuvqcnfk4.apps.googleusercontent.com';
// Enter one or more authorization scopes. Refer to the documentation for
// the API or https://developers.google.com/people/v1/how-tos/authorizing
// for details.
var discoveryDocs = 'https://www.googleapis.com/discovery/v1/apis/tagmanager/v2/rest';

var scopes = 'profile https://www.googleapis.com/auth/tagmanager.readonly';

var accountId = "";
var containerId = "";
var workspaceId = "";
var triggersDefined = [];

var apiToken = "";
var gapiOK = false;

function handleClientLoad(callback) {
  // Load the API client and auth2 library
  gapi.load('client:auth2', initClient);
}

function initClient () {
  gapi.client.init({discoveryDocs: [discoveryDocs]
  }).then(function (response) {
      gapiOK = true;
  }, function (reason) {
  });
}

function selectAccount(callback) {
  if (! gapiOK) {callback(null)};
  gapi.client.request({path: "https://www.googleapis.com/tagmanager/v2/accounts", headers: {'Authorization': 'Bearer ' + apiToken}}).then(function (response) {
    var accounts = response.result.account;
    callback(accounts);
  }, function (reason) {
    console.log(reason);
  });
}

function selectContainer (acid, callback) {
  if (! gapiOK) {callback(null)};
  accountId = acid;
  gapi.client.request({path: "https://www.googleapis.com/tagmanager/v2/accounts/" + accountId + "/containers", headers: {'Authorization': 'Bearer ' + apiToken}}).then(function (response) {
    var containers = response.result.container;
    callback(containers);
  }, function (reason) {
    console.log(reason);
  });
}

function selectWorkspace (ccid, callback) {
  if (! gapiOK) {callback(null)};
  containerId = ccid;
  gapi.client.request({path: "https://www.googleapis.com/tagmanager/v2/accounts/" + accountId + "/containers/" + containerId + "/workspaces", headers: {'Authorization': 'Bearer ' + apiToken}}).then(function (response) {
    var workspaces = response.result.workspace;
    callback(workspaces);
  }, function (reason) {
    console.log(reason);
  });
}

function loadGTMReference (event) {
  workspaceId = event.target.value;

  gapi.client.request({path: "https://www.googleapis.com/tagmanager/v2/accounts/" + accountId + "/containers/" + containerId + "/workspaces/" + workspaceId, headers: {'Authorization': 'Bearer ' + apiToken}}).then(function (response) {
    containerRef.textContent = response.result.publicId;
    getTagsAndTriggers();
  }, function (reason) {
    console.log(reason);
  });
}

function getTagsAndTriggers(contid, callback) {
  if (! gapiOK) {callback(null)};
  workspaceId = contid;
  triggersDefined.splice(0, triggersDefined.length);
  gapi.client.request({path: "https://www.googleapis.com/tagmanager/v2/accounts/" + accountId + "/containers/" + containerId + "/workspaces/" + workspaceId + "/triggers", headers: {'Authorization': 'Bearer ' + apiToken}}).then(function (response) {
    triggers = response.result.trigger;
    for (var item in triggers) {
      if (triggers[item].type == "customEvent") {
        triggersDefined.push({"name": triggers[item].name, "event": "custom", "customEvent": triggers[item].customEventFilter[0].parameter[1].value});
      }
    }
    chrome.storage.local.set({"GTMTriggers": triggersDefined});
    callback(triggersDefined);
  }, function (reason) {
    console.log(reason);
  });
}

function timeoutLoop(fn, reps, delay) {
  if (reps > 0)
    setTimeout(function() {
                 fn();
                 if (! gapiOK) timeoutLoop(fn, reps-1, delay);
  }, delay);
}

function loadGTMApi(callback) {
  chrome.identity.getAuthToken({ 'interactive': true }, function(token) {
    apiToken = token;
    handleClientLoad();
  });

  timeoutLoop(callback, 10, 1000);
}
