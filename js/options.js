/*
* Options handler
*/
'use strict';

function save_options() {
  var enableDTM = document.getElementById('enableDTM').checked;
  var enableGTM = document.getElementById('enableGTM').checked;

  chrome.storage.sync.set({
    enableDTM: enableDTM,
    enableGTM: enableGTM
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions() {
  if (dclhandler) { document.removeEventListener('DOMContentLoaded', restoreOptions); }

  chrome.storage.sync.get({
    enableDTM: true,
    enableGTM: false
  }, function(items) {
    var el = document.getElementById('enableDTM');
    console.log(el);
    el.checked = items.enableDTM;
    document.getElementById('enableGTM').checked = items.enableGTM;
  });
  document.getElementById('saveoptions').addEventListener('click',  save_options);
}

var dclhandler = false;
if (document.readyState !== 'loading') {
    restoreOptions();
} else {
    dclhandler = true;
    document.addEventListener('DOMContentLoaded', restoreOptions);
}
